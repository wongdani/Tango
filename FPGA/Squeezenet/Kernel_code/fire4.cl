#include<clc.h>
__kernel void __attribute__ ((reqd_work_group_size(55,1,1)))fire4(__global float *fire3_Features,__global float *fire4squeeze1x1_Weights_HW,__global float *fire4squeeze1x1_Features,__global float *fire4expand1x1_Weights_HW,__global float *fire4expand3x3_Weights_HW,__global float *fire4_Features)
	{
	int x = get_local_id(0);
	int y = get_group_id(0);
	float Features1 = 0;
	float Features2 = 0;
	float Features3 = 0;

	for(int f=0; f<32; f++)
	{
		Features1 = 0;
		for(int n=0; n<128; n++)
		{
               		Features1+= fire3_Features[n*55*55 + x*55 + y]*fire4squeeze1x1_Weights_HW[f*128+n];
		}
		//ReLU activation function computation
		if(Features1<0)
			Features1 = 0;
		fire4squeeze1x1_Features[f*55*55 + x*55 + y] = Features1;
	}

	barrier(CLK_LOCAL_MEM_FENCE);

	for(int f=0; f<128; f++)
	{
		Features2 = 0;
		for(int n=0; n<32; n++)
		{
			float result = 0;
               		result = fire4squeeze1x1_Features[n*55*55 + x*55 + y]*fire4expand1x1_Weights_HW[f*32+n];
			Features2+= result;
		}
		//ReLU activation function computation
		if(Features2<0)
			Features2 = 0;
		fire4_Features[f*55*55 + x*55 + y] = Features2;
	}
	barrier(CLK_LOCAL_MEM_FENCE);
	fire4_Features=fire4_Features+(55*55*128);

	for(int f=0; f<128; f++)
		{
			Features3 = 0;
			for(int n=0; n<32; n++)
			{	float result = 0;
					for(int i = x-1; i<=x+1; i++)
					{
	    					for(int j=y-1; j<=y+1; j++)
	    					{
							int x_index = i-x+1;
							int y_index = j-y+1;
							int m = (y_index)+(x_index)*3;
	         					if(i<0 || j<0)
							{
								result+=0;
							}
	         					else if(j>54 || i>54)
							{
								result+=0;
							}
	         					else
							{
	               						result+= fire4squeeze1x1_Features[n*55*55 + i*55 + j]*fire4expand3x3_Weights_HW[m+f*9*32+n*9];
							}
						}
					}
					Features3 += result;
			}
			//ReLU activation function computation
			if(Features3<0)
				Features3 = 0;
			fire4_Features[f*55*55 + x*55 + y] = Features3;
		}

}
