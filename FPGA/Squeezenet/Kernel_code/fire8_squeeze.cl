__kernel void __attribute__ ((reqd_work_group_size(27,1,1))) fire8_squeeze(__global float *fire8squeeze1x1_Weights_hw, __global float *fire8expand1x1_Weights_hw, __global float *fire8expand3x3_Weights_hw, __global float *fire7_Features, __global float *fire8squeeze1x1_Features, __global float *fire8_Features)
		{
	int x = get_local_id(0);
	int y = get_group_id(0);


	float Features = 0;

	for(int f=0; f<64; f++)
	{
		Features = 0;
		for(int n=0; n<384; n++)
		{
			Features+= fire7_Features[n*27*27 + x*27 + y]*fire8squeeze1x1_Weights_hw[f*384+n];
		}
		//ReLU activation function computation
		if(Features<0)
			Features = 0;
		fire8squeeze1x1_Features[f*27*27 + x*27 + y] = Features + fire8squeeze1x1_Weights_hw[24576 + f];
	}

}
